import { Container } from "inversify";
import * as express from 'express';
import CarDAO from "../app/car/dao/CarDAO";
import TYPES from "./TYPES";

/**
 * Register all the controllers here
 */
import "../app/controllers/CarController";
// ***

import ICarDAO from "../app/car/dao/ICarDAO";
import IJSONValidator from "../app/car/IJSONValidator";
import JSONValidator from "../app/car/JSONValidator";
import ServerApp from "../ServerApp";
import ValidateAPIKey from "../middleware/ValidateAPIKey";

const serverContainer = new Container({ defaultScope: "Singleton" });

serverContainer.bind<ICarDAO>(TYPES.CarDAO).to(CarDAO);
serverContainer.bind<IJSONValidator>(TYPES.JSONValidator).to(JSONValidator);
serverContainer.bind<ServerApp>(TYPES.SERVERAPP).to(ServerApp);

serverContainer.bind<string>(TYPES.SERVERPORT).toConstantValue(process.env.PORT as string);
serverContainer.bind<string>(TYPES.ENVIRONMENT).toConstantValue(process.env.NODE_ENV as string);
serverContainer.bind<string>(TYPES.MONGOURI).toConstantValue(process.env.MONGO_URI as string);

// container.bind<express.RequestHandler>('CustomMiddleware').toConstantValue(function customMiddleware(req: any, res: any, next: any) {
//     req.user = {
//         password: 'bar',
//         username: 'foo'
//     };
//     next();
// });

serverContainer.bind<express.RequestHandler>('ValidateAPIKey').toConstantValue( new ValidateAPIKey().validate);

export default serverContainer;
