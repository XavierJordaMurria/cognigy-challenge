const TYPES = {
  CarDAO: Symbol.for("CarDAO"),
  JSONValidator: Symbol.for("JSONValidator"),
  SERVERPORT: Symbol.for("SERVERPORT"),
  ENVIRONMENT: Symbol.for("ENVIRONMENT"),
  MONGOURI: Symbol.for("MONGOURI"),
  SERVERAPP: Symbol.for("SERVERAPP"),
};

export default TYPES;
