import ServerApp from "./ServerApp";

(async () => {
  const serverApp = new ServerApp(process.env.NODE_ENV as string);

  await serverApp.initialize(
    Number(process.env.PORT),
    process.env.MONGO_URI as string);
})()
