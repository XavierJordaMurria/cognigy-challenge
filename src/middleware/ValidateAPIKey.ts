import express from 'express';
import { StatusCodes } from 'http-status-codes';

/**
 * Middleware used for the x-api-key validation.
 */
export default class ValidateAPIKey {

  /**
   * This middelware method check that a given request contains an Authorization header with the correct x-api-key
   * secret.
   * @param req 
   * @param res 
   * @param next 
   * @returns 
   */
  public async validate(req: express.Request, res: express.Response, next: express.NextFunction): Promise<express.Response> {
    if (req.headers.authorization) {
      try {
        const authorization = req.headers.authorization.split(' ');
        if (authorization[0] !== 'x-api-key' || authorization[1] !== process.env.X_API_KEY_SECRET as string) {
          return res.status(StatusCodes.UNAUTHORIZED).send();
        }
        next();
        return res;

      } catch (err) {
        return res.status(StatusCodes.FORBIDDEN).send();
      }
    } else {
      return res.status(StatusCodes.UNAUTHORIZED).send();
    }
  }
}
