import mongoose from "mongoose";

/**
 * Connect mongoose with mongo
 */
export default function(url: string | undefined): Promise<unknown> {
  return new Promise<unknown>((resolve, reject) => {
    if (url) {
      const connect = (): void => {
        mongoose.set("useNewUrlParser", true);
        mongoose.set("useFindAndModify", false);
        mongoose.set("useCreateIndex", true);
        mongoose.set("useUnifiedTopology", true);

        mongoose.connect(url as string, { useNewUrlParser: true }).then(
          () => {
            console.info(`Mongoose connected to ${url}`);
            resolve(true);
          },
          (err: unknown) => {
            console.error(`Error connecting mongoose ${err}`);
            reject(err);
          }
        );
      }

      connect();
      mongoose.connection.on("disconnected", connect);
    } else {
      console.error("Missing mongo url...");
      reject(new Error("missing mongo url"));
    }
  });
}
