import "dotenv/config";
import "reflect-metadata";
import * as express from "express";
import { InversifyExpressServer } from "inversify-express-utils";
import morgan from "morgan";
import helmet from "helmet";
import serverContainer from "./ioc/ContainerBinder";
import connectMongoose from "./shared/connectMongoose";

/**
 * Entry point class, and set up of the express server
 */
export default class ServerApp {

  private readonly env: string;

  constructor(
    env: string,
  ) {
    this.env = env;
  }

  public async initialize(
    port: number,
    mongoURI: string): Promise<express.Application> {
    const server = new InversifyExpressServer(serverContainer);

    server.setConfig(app => {
      app.use(express.json());
    });

    const app = server.build();

    // Show routes called in console during development
    if (this.env === "development") {
      app.use(morgan("dev"));
    }

    // Security
    if (this.env === "production") {
      app.use(helmet());
    }

    app.listen(port, async () => {
      await connectMongoose(mongoURI as string);
      console.log(`Application running on port ${port}`);
    });

    return app;
  }
}