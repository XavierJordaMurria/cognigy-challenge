
export default interface ICar {
    carId: string;
    plate: string;
    brand: string;
    color: string;
    year:  number;
  }