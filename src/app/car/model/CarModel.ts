import { Schema, model, Document } from "mongoose";
import * as mongoose from 'mongoose';
import ICar from "./ICar";

export interface ICarModel extends ICar, Document { }

export const carSchema = new Schema<ICarModel>(
  {
    carId: { type: String, required: true, unique: true },
    plate: { type: String, required: true, unique: true },
    brand: { type: String, required: true },
    color: { type: String, required: true },
    year: { type: String, required: true },
  },
  {
    timestamps: true
  }
);

export default mongoose.model("CarModel", carSchema, "cars");