//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

//Require the dev-dependencies
import "reflect-metadata";
import chai from 'chai';
import 'mocha';
import JSONValidator from './JSONValidator';

let should = chai.should();

//Our parent block
describe('JSONValidator', () => {


    // Init the mongo handler
    before(async () => {
        console.log("before");
    });

    after(async () => {
        console.log("after");
    });

    /*
     * Test JSONValidator all fields compulsory
     */
    describe('JSONValidator all fields compulsory', () => {
        it('it should be correct data type', () => {
            const data = {
                carId: "1234125wsrq1345",
                plate: "8965dvr",
                brand: "volvo",
                color: "red",
                year: 1990
            };

            const validator = new JSONValidator();
            const vData = validator.valiateData(data, false);
            chai.expect(vData).to.be.true;
        });

        it('it should NOT be correct data type', () => {
            // missing one compulsory field (color)
            const data = {
                carId: "1234125wsrq1345",
                plate: "8965dvr",
                brand: "volvo",
                year: 1990
            };

            const validator = new JSONValidator();
            const vData = validator.valiateData(data, false);
            chai.expect(vData).to.be.false;
        });

        it('it should NOT be correct data type, one extra field', () => {
            // missing one compulsory field (color)
            const data = {
                carId: "1234125wsrq1345",
                plate: "8965dvr",
                brand: "volvo",
                color: "red",
                year: 1990,
                size: "big"
            };

            const validator = new JSONValidator();
            const vData = validator.valiateData(data, false);
            chai.expect(vData).to.be.false;
        });
    });

    /*
    * Test JSONValidator all fields optional
    */
    describe('JSONValidator all fields optional, isPartial=TRUE', () => {
        it('it should be correct data type, all optional parameter', () => {
            const data = {
                carId: "1234125wsrq1345",
                plate: "8965dvr",
                brand: "volvo",
                color: "red",
                year: 1990
            };

            const validator = new JSONValidator();
            const vData = validator.valiateData(data, true);
            chai.expect(vData).to.be.true;
        });

        it('it should be correct data type just 2 fields', () => {
            const data = {
                carId: "1234125wsrq1345",
                plate: "8965dvr",
            };

            const validator = new JSONValidator();
            const vData = validator.valiateData(data, true);
            chai.expect(vData).to.be.true;
        });

        it('it should NOT be correct data type, optional wrong parameter', () => {
            const data = {
                carIdFake: "1234125wsrq1345",
            };

            const validator = new JSONValidator();
            const vData = validator.valiateData(data, true);
            chai.expect(vData).to.be.false;
        });

        it('it should NOT be correct data type, optional wrong parameter extra', () => {
            const data = {
                carIdFake: "1234125wsrq1345",
                carId: "1234125wsrq1345",
                plate: "8965dvr",
                brand: "volvo",
                color: "red",
                year: 1990
            };

            const validator = new JSONValidator();
            const vData = validator.valiateData(data, true);
            chai.expect(vData).to.be.false;
        });
    });
});