import ICar from "./model/ICar";

export default interface IJSONValidator {
    valiateData(data: unknown, isPartial: boolean): data is ICar;
}