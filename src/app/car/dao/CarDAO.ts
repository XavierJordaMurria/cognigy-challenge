import { injectable, unmanaged } from "inversify";
import { Model, Schema } from "mongoose";
import CarModel, { ICarModel } from "../model/CarModel";
import ICar from "../model/ICar";
import ICarDAO from "./ICarDAO";

/**
 * Data Access Object DAO to interact with the car model
 */
@injectable()
export default class CarDAO implements ICarDAO {

  private _model: Model<Document>;

  private _modelSchema: Schema;

  _defaultDB: string;

  constructor(@unmanaged() model: Model<Document>, @unmanaged() modelSchema: Schema) {
    this._model = model;
    this._modelSchema = modelSchema;
    this._defaultDB = process.env.MONGO_DATABASE as string;
  }

  async insert(data: ICar): Promise<ICar> {
    const result = await CarModel.create(data);
    return result.toObject();
  }

  async getCount(): Promise<number> {
    return CarModel.countDocuments();
  }

  async findOne(_id: string): Promise<ICarModel | null> {
    const result = await CarModel.findById(_id);
    return result;
  }

  async update(_id: string, data: Partial<ICar>): Promise<ICarModel | null> {
    const result = await CarModel.findByIdAndUpdate(_id, { ...data as Record<string, unknown> }, { new: true });
    return result;
  }

  async delete(_id: string): Promise<ICarModel | null> {
    const result = await CarModel.findByIdAndDelete(_id);
    return result;
  }
}
