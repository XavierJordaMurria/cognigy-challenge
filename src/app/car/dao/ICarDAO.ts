import ICar from "../model/ICar";

export default interface ICarDAO {
  insert(data: ICar): Promise<ICar>;
  findOne(_id: string): Promise<ICar | null>;
  getCount(): Promise<number>;
  update(_id: string, data: Partial<ICar>): Promise<ICar | null>;
  delete(_id: string): Promise<ICar | null>;
}
