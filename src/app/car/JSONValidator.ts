import Ajv, { JSONSchemaType, ValidateFunction } from "ajv";
import { injectable } from "inversify";
import ICar from "./model/ICar";

const ajv = new Ajv();

/**
 * This class is a JSON validator using the ajv module. It is used to validate the data
 * received through the API.
 */
@injectable()
export default class JSONValidator {

    private readonly schemaPostPut: JSONSchemaType<ICar> = {
        type: "object",
        properties: {
            carId: { type: "string" },
            plate: { type: "string" },
            brand: { type: "string" },
            color: { type: "string" },
            year: { type: "integer" },
        },
        required: ["carId", "plate", "brand", "color", "year"],
        additionalProperties: false
    }

    private readonly schemaPatch: JSONSchemaType<Partial<ICar>> = {
        type: "object",
        properties: {
            carId: { type: "string", nullable: true },
            plate: { type: "string", nullable: true },
            brand: { type: "string", nullable: true },
            color: { type: "string", nullable: true },
            year: { type: "integer", nullable: true },
        },
        required: [],
        additionalProperties: false
    }

    private validatePostPut: ValidateFunction<ICar>;

    private validatePatch: ValidateFunction<Partial<ICar>>;

    constructor() {
        // Complie validation once and reuse them later to improve performance
        this.validatePostPut = ajv.compile<ICar>(this.schemaPostPut);
        this.validatePatch = ajv.compile<Partial<ICar>>(this.schemaPatch);
    }

    public valiateData(data: unknown, isPartial = true): data is ICar {

        const validate = isPartial ? this.validatePatch : this.validatePostPut;

        if (validate(data)) {
            return true
        }

        console.error(validate.errors)
        return false
    }
}