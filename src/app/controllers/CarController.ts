import * as express from 'express';
import {
  controller,
  response,
  request,
  httpPost,
  httpGet,
  httpDelete,
  httpPatch,
  requestParam
} from "inversify-express-utils";
import {
  StatusCodes,
} from 'http-status-codes';
import { Response, Request } from "express";
import { inject } from "inversify";
import TYPES from "../../ioc/TYPES";
import IJSONValidator from "../car/IJSONValidator";
import ICar from "../car/model/ICar";
import ICarDAO from "../car/dao/ICarDAO";
import ValidateAPIKey from '../../middleware/ValidateAPIKey';

/**
 * Controller class to interact with the car's API
 */
@controller("/car", new ValidateAPIKey().validate)
export default class CarController {
  private carDAO: ICarDAO;

  private jsonValidator: IJSONValidator;

  constructor(
    @inject(TYPES.CarDAO) carDAO: ICarDAO,
    @inject(TYPES.JSONValidator) jsonValidator: IJSONValidator) {
    this.carDAO = carDAO;
    this.jsonValidator = jsonValidator;
  }

  /**
   * GET /health-check 
   * @returns simple string showing that the server and the car controller are up and running
   */
  @httpGet("/health-check")
  private healthCheck(): string {
    return "server running and car controller on!";
  }

  /**
   * GET /meta-data
   * @param res 
   * @returns the number of cars in db.
   */
  @httpGet("/meta-data")
  private async getCarsMetaData(@response() res: Response,): Promise<Response> {
    try {
      const carsCount = await this.carDAO.getCount();
      return res.send({ count: carsCount });
    }
    catch (err) {
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({ error: err.message });
    }
  }

  /**
   * POST / . API to add a new car in db
   * @param res 
   * @param req 
   * @returns the created car
   */
  @httpPost("/")
  private async newCar(
    @response() res: Response,
    @request() req: Request
  ): Promise<Response> {

    if (!this.jsonValidator.valiateData(req.body, false)) {
      return res.status(StatusCodes.BAD_REQUEST).json({ error: "wrong parameters" });
    }

    const data: ICar = req.body as ICar;
    try {
      const newCar = await this.carDAO.insert(data);
      return res.status(StatusCodes.CREATED).send(newCar);
    } catch (err) {
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({ error: err.message });
    }
  }

  /**
   * GET /:id This API gets a car object given car mongoId
   * @param id mongoID for the car to retrieve
   * @param req 
   * @param res 
   * @returns 
   */
  @httpGet("/:id")
  private async getCarById(
    @requestParam("id") id: string,
    @request() req: Request,
    @response() res: Response): Promise<Response> {

    if (!id) {
      return res.status(StatusCodes.BAD_REQUEST).json({ error: "wrong parameters" });
    }

    try {
      const result = await this.carDAO.findOne(id);
      return res.send(result);
    } catch (err) {
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({ error: err.message });
    }
  }

  /**
   * PATH /:id Given a car's mongoId, it updates the passed values in the body
   * @param id car's mongoId to path
   * @param res 
   * @param req 
   * @returns 
   */
  @httpPatch("/:id")
  private async patchCar(
    @requestParam("id") id: string,
    @response() res: Response,
    @request() req: Request
  ): Promise<Response> {

    if (!id || !this.jsonValidator.valiateData(req.body, true)) {
      return res.status(StatusCodes.BAD_REQUEST).json({ error: "wrong parameters" });
    }

    try {
      const result = await this.carDAO.update(id, req.body);
      return res.send(result);
    } catch (err) {
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({ error: err.message });
    }
  }

  /**
   * DELETE /:id API to deleve a car given its mongoId
   * @param id car's mongoId to be remove
   * @param res 
   * @returns 
   */
  @httpDelete("/:id")
  private async deleteCar(
    @requestParam("id") id: string,
    @response() res: Response,
  ): Promise<Response> {

    if (!id) {
      return res.status(StatusCodes.BAD_REQUEST).json({ error: "wrong parameters" });
    }

    try {
      const result = await this.carDAO.delete(id);
      return res.send(result);
    } catch (err) {
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({ error: err.message });
    }
  }
}
