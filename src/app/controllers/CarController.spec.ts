//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

import { MongoMemoryServer } from 'mongodb-memory-server';

//Require the dev-dependencies
import chai, { expect } from 'chai';
import chaiHttp = require('chai-http');
import 'mocha';
import CarModel from '../car/model/CarModel';
import ServerApp from '../../ServerApp';
import { connection } from 'mongoose';

chai.use(chaiHttp);

//Our parent block
describe('Cars', function () {
  this.timeout(15000);
  let mongoUri: string = '';
  let mongod: MongoMemoryServer;
  let app: Express.Application;

  // Init the mongo handler
  before(async () => {
    console.log("before");
    mongod = await MongoMemoryServer.create();
    mongoUri = mongod.getUri();

    const serverApp = new ServerApp(process.env.NODE_ENV as string);

    app = await serverApp.initialize(Number(process.env.PORT),
      mongoUri);
  });

  after(async () => {
    console.log("after");
    mongod.stop();
  });

  beforeEach(() => { //Before each test we empty the database
    console.log("beforeEach");

    CarModel.remove({}, (err) => {
    });
  });
  /*
    * Test the /GET route
    */
  describe('/GET Health check', () => {
    it('it should GET Health Check test method', () => {
      chai.request(app)
        .get('/car/health-check')
        .set('Authorization', 'x-api-key this-is-my-api-key')
        .end((err, res) => {
          res.should.have.status(200);
          expect(res.text).to.eql("server running and car controller on!");
        });
    });
  });

  describe('car/ POST', () => {
    it('it should add new car on DB', () => {
      const newCar = {
        carId: "1234125wsrq1345",
        plate: "8965dvr",
        brand: "volvo",
        color: "red",
        year: 1990
      };
      chai.request(app)
        .post('/car/')
        .set('Authorization', 'x-api-key this-is-my-api-key')
        .send(newCar)
        .end((err, res) => {
          res.should.to.be.json;
          res.should.have.status(201);
        });
    });

    it('it should fail adding new car with incorrect params, 1 less', () => {
      const wrongCarParams = {
        carId: "1234125wsrq1345",
        brand: "volvo",
        color: "red",
        year: 1990
      };
      chai.request(app)
        .post('/car/')
        .set('Authorization', 'x-api-key this-is-my-api-key')
        .send(wrongCarParams)
        .end((err, res) => {
          res.should.to.be.json;
          res.should.have.status(400);
        });
    });

    it('it should fail adding new car with incorrect params, extra', () => {
      const wrongCarParams = {
        carId: "1234125wsrq1345",
        plate: "8965dvr",
        brand: "volvo",
        color: "red",
        size: "big",
        year: 1990
      };
      chai.request(app)
        .post('/car/')
        .set('Authorization', 'x-api-key this-is-my-api-key')
        .send(wrongCarParams)
        .end((err, res) => {
          res.should.to.be.json;
          res.should.have.status(400);
        });
    });
  });

  describe('car/:id Get', () => {
    it('it should get a car from DB', async () => {
      const newCar = {
        carId: "1234125wsrq1345",
        plate: "8965dvr",
        brand: "volvo",
        color: "red",
        year: 1990
      };

      const insertedCar = await connection.collection('cars').insertOne(newCar);
      const insertedCarId = insertedCar.insertedId.toString()
      console.log(insertedCarId);
      chai.request(app)
        .get(`/car/${insertedCarId}`)
        .set('Authorization', 'x-api-key this-is-my-api-key')
        .send(newCar)
        .end((err, res) => {
          res.should.have.status(200);
        });
    });

    it('it should fail getting a car from DB, wrongId', async () => {
      const newCar = {
        carId: "1234125wsrq1345",
        plate: "8965dvr",
        brand: "volvo",
        color: "red",
        year: 1990
      };

      const insertedCar = await connection.collection('cars').insertOne(newCar);
      const wrongId = "6114b1c5b0eac64da8db5806";
      chai.request(app)
        .get(`/car/${wrongId}`)
        .set('Authorization', 'x-api-key this-is-my-api-key')
        .send(newCar)
        .end((err, res) => {
          expect(res.body).to.be.empty;
          res.should.have.status(200);
        });
    });

    it('it should fail getting a car from DB, wrong formated mongoId', async () => {
      const newCar = {
        carId: "1234125wsrq1345",
        plate: "8965dvr",
        brand: "volvo",
        color: "red",
        year: 1990
      };

      const insertedCar = await connection.collection('cars').insertOne(newCar);
      chai.request(app)
        .get(`/car/1`)
        .set('Authorization', 'x-api-key this-is-my-api-key')
        .send(newCar)
        .end((err, res) => {
          res.should.have.status(500);
        });
    });

    it('it should fail getting a car from DB, no Id', async () => {
      const newCar = {
        carId: "1234125wsrq1345",
        plate: "8965dvr",
        brand: "volvo",
        color: "red",
        year: 1990
      };

      const insertedCar = await connection.collection('cars').insertOne(newCar);
      chai.request(app)
        .get(`/car/`)
        .set('Authorization', 'x-api-key this-is-my-api-key')
        .send(newCar)
        .end((err, res) => {
          res.should.have.status(404);
        });
    });
  });


  describe('car/:id Patch', () => {
    it('it should Patch a car with ID', async () => {
      const newCar = {
        carId: "1234125wsrq1345",
        plate: "8965dvr",
        brand: "volvo",
        color: "red",
        year: 1990
      };

      const insertedCar = await connection.collection('cars').insertOne(newCar);
      const insertedCarId = insertedCar.insertedId.toString()

      const newData = {
        brand: "BMW",
        color: "black",
      }

      chai.request(app)
        .patch(`/car/${insertedCarId}`)
        .set('Authorization', 'x-api-key this-is-my-api-key')
        .send(newData)
        .end((err, res) => {
          expect(res.body.carId).to.equal(newCar.carId);
          expect(res.body.plate).to.equal(newCar.plate);
          expect(res.body.brand).to.equal(newData.brand);
          expect(res.body.color).to.equal(newData.color);
          expect(String(res.body.year)).to.equal(String(newCar.year));
          res.should.have.status(200);
        });
    });

    it('it should Patch a car with ID, empty new data', async () => {
      const newCar = {
        carId: "1234125wsrq1345",
        plate: "8965dvr",
        brand: "volvo",
        color: "red",
        year: 1990
      };

      const insertedCar = await connection.collection('cars').insertOne(newCar);
      const insertedCarId = insertedCar.insertedId.toString()

      const newData = {}

      chai.request(app)
        .patch(`/car/${insertedCarId}`)
        .set('Authorization', 'x-api-key this-is-my-api-key')
        .send(newData)
        .end((err, res) => {
          expect(res.body.carId).to.equal(newCar.carId);
          expect(res.body.plate).to.equal(newCar.plate);
          expect(res.body.brand).to.equal(newCar.brand);
          expect(res.body.color).to.equal(newCar.color);
          expect(String(res.body.year)).to.equal(String(newCar.year));
          res.should.have.status(200);
        });
    });

    it('it should fail Patching a car with no ID', async () => {
      const newCar = {
        carId: "1234125wsrq1345",
        plate: "8965dvr",
        brand: "volvo",
        color: "red",
        year: 1990
      };

      await connection.collection('cars').insertOne(newCar);
      const newData = {
        brand: "BMW",
        color: "black",
      }

      chai.request(app)
        .patch(`/car/`)
        .set('Authorization', 'x-api-key this-is-my-api-key')
        .send(newData)
        .end((err, res) => {
          res.should.have.status(404);
        });
    });

    it('it should fail Patching a car with invalid mongo ID', async () => {
      const newCar = {
        carId: "1234125wsrq1345",
        plate: "8965dvr",
        brand: "volvo",
        color: "red",
        year: 1990
      };

      await connection.collection('cars').insertOne(newCar);
      const insertedCarId = 'invaildMongoId';
      const newData = {
        brand: "BMW",
        color: "black",
      }

      chai.request(app)
        .patch(`/car/${insertedCarId}`)
        .set('Authorization', 'x-api-key this-is-my-api-key')
        .send(newData)
        .end((err, res) => {
          res.should.have.status(500);

        });
    });
  });

  describe('car/:id Delete', () => {
    it('it should Delete a car from DB', async () => {
      const newCar = {
        carId: "1234125wsrq1345",
        plate: "8965dvr",
        brand: "volvo",
        color: "red",
        year: 1990
      };

      const insertedCar = await connection.collection('cars').insertOne(newCar);
      const insertedCarId = insertedCar.insertedId.toString();

      chai.request(app)
        .delete(`/car/${insertedCarId}`)
        .set('Authorization', 'x-api-key this-is-my-api-key')
        .send(newCar)
        .end((err, res) => {
          res.should.have.status(200);

        });
    });

    it('it should fail deleting a car from DB, wrongId', async () => {
      const newCar = {
        carId: "1234125wsrq1345",
        plate: "8965dvr",
        brand: "volvo",
        color: "red",
        year: 1990
      };

      await connection.collection('cars').insertOne(newCar);
      const wrongId = "6114b1c5b0eac64da8db5806";
      chai.request(app)
        .delete(`/car/${wrongId}`)
        .set('Authorization', 'x-api-key this-is-my-api-key')
        .send(newCar)
        .end((err, res) => {
          expect(res.body).to.be.empty;
          res.should.have.status(200);

        });
    });

    it('it should fail deleting a car from DB, wrong formated mongoId', async () => {
      const newCar = {
        carId: "1234125wsrq1345",
        plate: "8965dvr",
        brand: "volvo",
        color: "red",
        year: 1990
      };

      await connection.collection('cars').insertOne(newCar);
      chai.request(app)
        .get(`/car/1`)
        .set('Authorization', 'x-api-key this-is-my-api-key')
        .send(newCar)
        .end((err, res) => {
          res.should.have.status(500);

        });
    });

    it('it should fail deleting a car from DB, no Id', async () => {
      const newCar = {
        carId: "1234125wsrq1345",
        plate: "8965dvr",
        brand: "volvo",
        color: "red",
        year: 1990
      };

      await connection.collection('cars').insertOne(newCar);
      chai.request(app)
        .get(`/car/`)
        .set('Authorization', 'x-api-key this-is-my-api-key')
        .send(newCar)
        .end((err, res) => {
          res.should.have.status(404);

        });
    });
  });

  describe('car/meta-data Get', () => {
    it('it should Get a cars collection meta-data from DB', async () => {
      const newCar = {
        carId: "1234125wsrq1345",
        plate: "8965dvr",
        brand: "volvo",
        color: "red",
        year: 1990
      };

      await connection.collection('cars').insertOne(newCar);

      chai.request(app)
        .get(`/car/meta-data`)
        .set('Authorization', 'x-api-key this-is-my-api-key')
        .send(newCar)
        .end((err, res) => {
          expect(res.body.count).to.be.equal(1);
          res.should.have.status(200);
        });
    });
  });
});