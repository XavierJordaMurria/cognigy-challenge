# cognigy-challenge


## Docker Run
runMe is an npm script that sets up the project and launches it with docker. 
For a fast launch process, just place your terminal into the project's directory and run ***npm run runMe*** 

## Project description
This section briefly hightlights the most relevant libraries/technologies used in this project.
This project uses Express as the web application framework. It also includes Inversify and Inversify-express-utils to manage the IOC in a more graceful way. In addition, the JSON schemas validation is done with Ajv.

For the project testing, Mocha is used as the testing framework and Chai/Chai-http are used for the assertions.
Last but not least, MongoMemoryServer is used for mocking a MongoDb.

## NPM scripts

#### init
npm run init, sets the project up, including copying environment files and installing dependencies 

#### runMe
npm run runMe, prepares the project and launches it with docker. 

#### start
#### dev
#### debug
npm run start/dev/debug, start the nodejs project with different flavours.

#### prebuild
#### build
#### postbuild
npm run build, and by extension prebuild/postbuild, compile the typescript project into javacript and place it 
into the ***dis*** directory

#### docker:db:up
#### docker:db:down
npm run docker:db:up/down, launch the mongoDb with a docker container.

#### docker:prune
#### docker:clean
npm run docker:prune/clean, remove and clean docker containers.

#### docker:build:img
npm run docker:build:img, builds a docker image containg the nodejs project.

#### docker:up
#### docker:down
npm run docker:up/down, start/stop the whole ecosystem (node app, mongoDb) with docker.

#### lint
#### lint:dev
#### lint:fix
npm run lin/dev/fix, these are the linting scripts to format the code.
 
#### test
#### test:debug
npm run test/:debug, these are the scripts used to run the tests.
